/**
 * Автопечать чеков-квитанций
 * ТОО "ЭнергияРК Рудный" Рудный ЕРЦ
 *
 * @date 2016-06-13
 * @author barbass1025@gmail.com
 *
 * Help:
 * http://www.java-forums.org/awt-swing/24048-jeditorpane-html-4-0-css-custom-tags.html
 */
package kz.erc10;

import java.awt.print.PrinterException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.print.PrintException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509ExtendedTrustManager;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.JEditorPane;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Автопечать чеков
 *
 * @author barbass1025@gmail.com (erc10.kz)
 */
public class ErcAutoprint {

    protected static String tmp_dir = "/tmp/";
    protected static String jar_dir = ".";
    protected static String AdobeReader = null;

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws javax.print.PrintException
     * @throws org.json.simple.parser.ParseException
     */
    public static void main(String[] args) throws FileNotFoundException, PrintException, IOException, ParseException, Exception {
        tmp_dir = System.getProperty("java.io.tmpdir");
        jar_dir = new File(".").getAbsolutePath();

        String url;

        // Добавляем логгирование
        FileHandler fh;
        fh = new FileHandler(jar_dir.concat("\\").concat(ErcAutoprint.class.getName()).concat(".xml"));
        Logger.getLogger(ErcAutoprint.class.getName()).addHandler(fh);

        if (args.length == 0) {
            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("Not args"));
            return;
        }
        String json = readMessage();
        //Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception(json));
        if (json.length() == 0) {
            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("Empty json"));
            return;
        }
        //Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("json: ".concat(json)));
        //Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("Args: ".concat(Arrays.toString(args))));

        getAdobeReader();

        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            url = (String) obj.get("receiptURL");
            // url = java.net.URLDecoder.decode((String) obj.get("receiptURL"), "UTF-8");

            //Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("Urls: ".concat(url)));
        } catch (ParseException ex) {
            //Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, new Exception("Json: ".concat(json)));
            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        try {
            String file = getFile(url);

            String msg = "{\"result\":\"0\"}";

            if (file.contains(".pdf")) {
                printPdf(file);
            } else if (file.contains(".html")) {
                String format;
                if (url.contains("&type=a5")) {
                    format = "a5";
                } else if (url.contains("&type=a4")) {
                    format = "a4";
                } else if (url.contains("&type=a6")) {
                    format = "a6";
                } else {
                    format = "a5";
                }

                printHtml(file, format);
            } else {
                throw new Exception("Error format");
            }

            writeMessage(msg);

        } catch (Exception ex) {
            String msg = "{\"result\":\"1\"}";
            writeMessage(msg);

            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Получение файла с сервера
     *
     * @param url_file
     * @return
     * @throws java.net.MalformedURLException
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.KeyManagementException
     */
    public static String getFile(String url_file) throws MalformedURLException, IOException, NoSuchAlgorithmException, KeyManagementException {
        // Если https, то отключаем проверку ssl-сертификатов
        if (url_file.contains("https://")) {
            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts;
            trustAllCerts = new TrustManager[]{new X509ExtendedTrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string, Socket socket) throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] xcs, String string, SSLEngine ssle) throws CertificateException {
                }
            }};

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }

        URLConnection connection;
        String contentType;
        String extension;

        try {
            URL url = new URL(url_file);
            connection = url.openConnection();
            connection.setDoOutput(true);
            contentType = connection.getContentType();
        } catch (IOException ex) {
            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        if (contentType.contains("text/html")) {
            extension = "html";
        } else if (url_file.contains("&format=html")) {
            extension = "html";
        } else if (url_file.contains("&format=pdf")) {
            extension = "pdf";
        } else if (contentType.contains("application/pdf")) {
            extension = "pdf";
        } else {
            extension = "pdf";
        }

        String file_name = tmp_dir.concat("ErcAutoprint").concat(Double.toString(Math.random())).concat(Double.toString(Math.random())).concat(".").concat(extension);

        InputStream in = connection.getInputStream();

        OutputStream out = new FileOutputStream(file_name);

        byte[] buf = new byte[512];
        int read;
        while ((read = in.read(buf)) > 0) {
            out.write(buf, 0, read);
        }
        return file_name;
    }

    /**
     * Определяем, где находится Adobe Reader
     *
     * @throws java.lang.Exception
     */
    public static void getAdobeReader() throws Exception {
        String[] paths = new String[]{
            "C:\\Program Files (x86)\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe",
            "C:\\Program Files (x86)\\Adobe\\Acrobat Reader\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Acrobat Reader\\Reader\\AcroRd32.exe",
            "C:\\Program Files\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe",
            "C:\\Program Files (x86)\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe"
        };

        for (String path : paths) {
            File f = new File(path);
            if (f.exists()) {
                AdobeReader = path;
                break;
            }
        }

        if (AdobeReader == null) {
            throw new Exception("Not found Adobe Reader");
        }
    }

    /**
     * Печать pdf-файла
     *
     * @param file
     * @throws java.lang.Exception
     */
    public static void printPdf(String file) throws Exception {
        String commandAdobeReader = AdobeReader.concat(" /N /T ").concat(file);
        Runtime.getRuntime().exec(commandAdobeReader);
    }

    /**
     * Печать html-файла
     *
     * @param file
     * @param format
     * @throws java.lang.Exception
     */
    public static void printHtml(String file, String format) throws Exception {
        //URL urlToPage = new File(file).toURI().toURL();

        JEditorPane editorPane = new JEditorPane();

        editorPane.setContentType("text/html; charset=UTF-8");
        editorPane.setEditable(false);
        editorPane.setVisible(false);

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        String line = reader.readLine();
        String html = "";
        while (line != null) {
            html += line;
            line = reader.readLine();
        }

        HTMLEditorKit hed = new HTMLEditorKit();
        Document doc = hed.createDefaultDocument();
        editorPane.setEditorKit(hed);
        editorPane.setDocument(doc);

        editorPane.setText(html);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        MediaSizeName msn;
        switch (format) {
            case "a4":
                /**
                 * 210, 297
                 */
                msn = MediaSizeName.ISO_A4;
                break;
            case "a5":
                /**
                 * 148, 210
                 */
                msn = MediaSizeName.ISO_A5;
                break;
            case "a6":
                /**
                 * 105, 148
                 */
                msn = MediaSizeName.ISO_A6;
                break;
            default:
                msn = MediaSizeName.ISO_A5;
                break;
        }
        attributeSet.add(msn);

        /*
        // Нет нужды
        MediaSize ms = MediaSize.getMediaSizeForName(msn);
        attributeSet.add(new MediaPrintableArea(
                0, // отступ слева
                0, // отступ сверху
                ms.getX(MediaPrintableArea.MM), // ширина
                ms.getY(MediaPrintableArea.MM), // высота
                MediaPrintableArea.MM
        ));*/
        // Печать результата в файл
        /*Writer writer = new FileWriter("C:\\Users\\ItRudnyERC\\Desktop\\java.html");
        editorPane.write(writer);
        writer.flush();
        writer.close();*/
        try {
            editorPane.print(null, null, false, PrintServiceLookup.lookupDefaultPrintService(), attributeSet, false);
        } catch (PrinterException ex) {
            // pass
        }
    }

    /**
     * Считывание данных из потока
     *
     * @return
     * @throws IOException
     */
    public static String readMessage() throws IOException {
        byte[] b = new byte[4];
        System.in.read(b);

        int size = getInt(b);

        b = new byte[size];
        System.in.read(b);

        return new String(b, "UTF-8");
    }

    /**
     * Вывод данных
     *
     * @param msg
     * @throws IOException
     */
    public static void writeMessage(String msg) throws IOException {
        System.out.write(getBytes(msg.length()));
        System.out.write(msg.getBytes("UTF-8"));
        System.out.flush();
    }

    public static int getInt(byte[] bytes) {
        return (bytes[3] << 24) & 0xff000000
                | (bytes[2] << 16) & 0x00ff0000
                | (bytes[1] << 8) & 0x0000ff00
                | (bytes[0] << 0) & 0x000000ff;
    }

    public static byte[] getLength() {
        int length = 0;
        byte[] bytes = new byte[4];
        try {
            System.in.read(bytes, 0, 4);
        } catch (IOException ex) {
            Logger.getLogger(ErcAutoprint.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bytes;
    }

    public static byte[] getBytes(int length) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte) (length & 0xFF);
        bytes[1] = (byte) ((length >> 8) & 0xFF);
        bytes[2] = (byte) ((length >> 16) & 0xFF);
        bytes[3] = (byte) ((length >> 24) & 0xFF);
        return bytes;
    }
}
